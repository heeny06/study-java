package com.shk.studyjava.service;

import com.shk.studyjava.model.Example1Item;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
public class Example1Service {
    /**
     * 샘플 데이터를 생성한다.
     *
     * @return Exampleitem 리스트
     */
    private List<Example1Item> settingData() {
        List<Example1Item> result = new LinkedList<>();

        Example1Item item1 = new Example1Item(); // Example1Item에 Example1Item item1을 생성한다
        item1.setName("홍길동"); // item1의 이름은 홍길동이다
        item1.setBirthDay(LocalDate.of(2022, 1, 1)); // item1의 생일은 2022년 1월 1일이다
        result.add(item1); // item1을 Example1Item에 넣는다

        Example1Item item2 = new Example1Item();
        item2.setName("홍길동2");
        item2.setBirthDay(LocalDate.of(2023, 2, 2));
        result.add(item2);

        Example1Item item3 = new Example1Item();
        item3.setName("홍길동3");
        item3.setBirthDay(LocalDate.of(2024, 3, 3));
        result.add(item3);

        Example1Item item4 = new Example1Item();
        item4.setName("홍길동4");
        item4.setBirthDay(LocalDate.of(2025, 4, 4));
        result.add(item4);

        Example1Item item5 = new Example1Item();
        item5.setName("홍길동5");
        item5.setBirthDay(LocalDate.of(2026, 5, 5));
        result.add(item5);

        return result;


    }
}
