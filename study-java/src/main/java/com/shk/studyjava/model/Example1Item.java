package com.shk.studyjava.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class Example1Item {
    private String name;
    private LocalDate birthDay;

}
